## 0. Préparation de la machine
```
[farixo@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=118 time=18.7 ms

[farixo@node1 ~]$ dig ynov.com
ynov.com.               5139    IN      A       92.243.16.143


[farixo@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.393 ms

[farixo@node2 ~]$ ping node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.380 ms
```
# I. Utilisateurs

## 1. Création et configuration

**Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :**



- Modification du fichier /etc/sudoers
```
## Allows people in group wheel to run all commands
%admins  ALL=(ALL)       ALL

## Same thing without a password
# %admins        ALL=(ALL)       NOPASSWD: ALL
```
## 2. SSH

```
drwx------. 2 farixo farixo   29 Sep 26 12:43 .ssh

-rw-------. 1 root   root   747 Sep 26 12:43 authorized_keys
Clé public :
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC55mPFtI0JN4MGiunQ4VCVy0B1vceFe8Qurjjvej03pqs0u1dOmuLxrZyXzH5rgl4qwS3hure1K8XKSlHW52k8ZD6dAmCrH+UHU4D4BXtlD29o6H6XU9fLlWb0nHTaX8aZ99pf9T7jwsDJlQ4VMGz34iKYRzWil89WlbyUrZhiVduz+2xI1Ggc4NcVHIFrTPctbvisC0VFF7Tnx4Hr/xrGYYMJNt/HtuN6vRFE9xGtjNgf5EJH+KZumWtgf0Ipa0ccpcGw775Re9Ddd9J7Csfx6YOaud6KR0yz/O2zx65Zab35IoEZ3qu0UGsGw2OwiQqtuyH29TXtcYUnnGf7Hwi2RpmhjeX+bJko4E/7QLXkAqvPWQe16sRcNbrvJEjBGCWNbiJVEMnsdPobeArq6jFHwkBb+2NR/73+/X2SWErzF1P3IzjjD4dX/h1hR8G6lig7/sx7Rbpf3jiZgkZhycpdRdPTpJ1tBUbKFTER8T+B4T2QsEOdAgXUoJHoBN2+6U8WzZ62rC5PdQTaY6uVTMrDaqoFuEiBul9CQ5jInBRIQhvkxzJKPiVwKkOUh8fOGxFzrkwfXeMb3Sgf52MsUHFkPLYLi30TN/ueloNG7CwKJtHKIxPIL4BSxsI+MPYAR7s4AE9nwsrI481o7qSBw9IvmuUwTC1jiAN0VscTANc1MQ== farix@LAPTOP-D1HQMJ9S
```
## II. Partitionnement

### 1. Préparation de la VM
```
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
```
### 2. Partitionnement

```
[farixo@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   1   0   0 wz--n- <3.00g <3.00g
  rl     1   2   0 wz--n- <7.00g     0
```
-  Utilisez LVM pour :
```
/dev/data/lv1 /mnt/part1 ext4 defaults 0 0
/dev/data/lv1 /mnt/part2 ext4 defaults 0 0
/dev/data/lv1 /mnt/part3 ext4 defaults 0 0
```

```
[farixo@node1 lvm]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
├─data-lv1  253:2    0    1G  0 lvm  /mnt/part3
└─data-lv2  253:3    0    1G  0 lvm
sdc           8:32   0    3G  0 disk
└─data-lv3  253:4    0    1G  0 lvm
sr0          11:0    1 1024M  0 rom
```
## III. Gestion de services

### 1. Interaction avec un service existant
```
[farixo@node1 /]$ systemctl is-active firewalld.service
active
[farixo@node1 /]$ systemctl is-enabled firewalld.service
enabled
```
### 2. Création de service

```
[farixo@node1 system]$ systemctl edit web.service --full --force
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

```
[farixo@node1 system]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: failed (Result: exit-code) since Sun 2021-09-26 16:52:10 CEST; 2min 22s ago
 Main PID: 2507 (code=exited, status=203/EXEC)

Sep 26 16:52:10 node1.tp1.b2 systemd[1]: Started Very simple web service.
Sep 26 16:52:10 node1.tp1.b2 systemd[1]: web.service: Main process exited, code=exited, status=203/EXEC
Sep 26 16:52:10 node1.tp1.b2 systemd[1]: web.service: Failed with result 'exit-code'.
```